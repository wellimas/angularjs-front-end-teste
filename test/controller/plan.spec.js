describe('Plans', function () {
  var Plans;

  beforeEach(angular.mock.module('mainApp'));

  beforeEach(inject(function (_Plans_) {
    Plans = _Plans_;
  }));

  it('Plataformas existe', function () {
    expect(Plans).toBeDefined();
  });

  describe('.all()', function () {
    it('Lista existe', function () {
      expect(Plans.all).toBeDefined();
    });
  });

  it('O método findBySku existe', function () {
    expect(Plans.findBySku).toBeDefined();
  });

  describe('.findBySku()', function () {
    it('O método findBySku existe', function () {
      expect(Plans.findBySku).toBeDefined();
    });
  });

});

