describe('Plataforms', function () {
  var Plataforms;

  beforeEach(angular.mock.module('mainApp'));

  beforeEach(inject(function (_Plataforms_) {
    Plataforms = _Plataforms_;
  }));

  it('Plataformas existe', function () {
    expect(Plataforms).toBeDefined();
  });

  describe('.all()', function () {
    it('Lista existe', function () {
      expect(Plataforms.all).toBeDefined();
    });
  });

  it('O método findBySku existe', function () {
    expect(Plataforms.findBySku).toBeDefined();
  });

  describe('.findBySku()', function () {
    it('O método findBySku existe', function () {
      expect(Plataforms.findBySku).toBeDefined();
    });
  });

});

