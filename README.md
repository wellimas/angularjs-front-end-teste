**Desafio para Vaga: Front-End**

Aplicação de teste em AngularJS que consome serviços externos e gerencia as informações possibilitando ao usuário navegar pelos respectivos planos (vinculados pelo sku) de cada plataforma onde na última tela possui um formulário simples que ao finalizar printa as informações no console do navegador.

---

## Pilha Tecnológica:

Rode a aplicação em desenvolvimento utilizando o Browser Sync.

1. AngularJS 1.7.7
2. Gulp 4.0.0
3. Bootstrap 3.2.0
4. JSHint
5. Browser Sync
5. Karma
5. Jasmine

---

## Para Rodar a Aplicação e Testar

Você precisa ter o **Node.js** instalado para poder baixar o Gulp e também o Karma/Jasmine com o npm, isso caso queiram usar o Browser Sync e rodar os testes.

Rodando a aplicação usando os arquivos da pasta **dist**:

Esses arquivos da pasta dist estão prontos para fazer o deploy e basta copiar para um local acessível em seu web server (nginx, apache, wamp, xampp ou iis)

Rodando a aplicação usando o **Node.js**:

1. Primeiramente **Clone** o projeto
2. Acesse a pasta do projeto clonado e digite no terminal **npm install**
3. E após baixar os pacotes digite no terminal **gulp**
4. Será gerado o build e automaticamente e carregará a aplicação em seu navegador na url: http://localhost:3000
5. Para testar com o Karma/Jasmine acesse a pasta do projeto clonado, depois de ter baixados as dependências com o **npm install** digite no terminal **karma start**

---
## Prints das Telas:

Home
![Tela 01](prints/passo-1.jpg)

---
Selecionando a plataforma
![Scheme](prints/passo-2.jpg)

---
Selecionando o plano
![Scheme](prints/passo-3.jpg)

---
Formulário Simples
![Scheme](prints/passo-4.jpg)

---
Apresentação dos dados no console
![Scheme](prints/passo-5.jpg)

---

