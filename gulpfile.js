var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var jshint = require('gulp-jshint');
var del = require('del'); // rm -rf
var cleanCSS = require('gulp-clean-css');
var browser = require('browser-sync').create();
var historyApiFallback = require('connect-history-api-fallback');
var port = process.env.SERVER_PORT || 3000;

var toDelete = [
  'dist/**/*',
]
 
gulp.task('clean', function() {
  return del(toDelete); // rm -rf
});

gulp.task('copyFont', done => {  
  return gulp.src('app/assets/fonts/**/*')
    .pipe(gulp.dest('dist/assets/fonts'))
})

gulp.task('copyScripts', function() {
  return gulp.src('app/assets/scripts/**/*')
    .pipe(gulp.dest('dist/assets/scripts'))
})

gulp.task('copyStyles', function() {
  return gulp.src('app/assets/styles/**/*')
    .pipe(gulp.dest('dist/assets/styles'))
})

gulp.task('copyImages', function() {
  return gulp.src('app/assets/images/**/*')
    .pipe(gulp.dest('dist/assets/images'))
})

gulp.task('copyController', function() {
  return gulp.src('app/controller/**/*')
    .pipe(gulp.dest('dist/controller'))
})

gulp.task('copyModel', function() {
  return gulp.src('app/model/**/*')
    .pipe(gulp.dest('dist/model'))
})

gulp.task('copyView', function() {
  return gulp.src('app/view/**/*')
    .pipe(gulp.dest('dist/view'))
})

gulp.task('copyRoot', function() {
  return gulp.src('app/*')
    .pipe(gulp.dest('dist'))
})

// Minify
gulp.task('minifyStylesheets', function() {
  var stream = gulp.src('app/assets/styles/**/*')
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('dist/assets/styles'))
    .pipe(rename('styles.min.css'))
    .pipe(cleanCSS({ compatibility: 'ie8' }))
    .pipe(gulp.dest('dist/assets/styles'));
  return stream;
})

gulp.task('jshint', function(done) { 
  gulp.src('app/controller/**/*.js')
      .pipe(jshint())
      .pipe(jshint.reporter('default'));
      done();
});

gulp.task('browser', function() {
  browser.init({
    server: 'dist/',
    port: port,
    middleware: [historyApiFallback()]
  });

  gulp.watch('app/assets/scripts/**/*', gulp.parallel('copyScripts'))
    .on('change', browser.reload);

  gulp.watch('app/controller/**/*', gulp.parallel('copyController'))
    .on('change', browser.reload); 
  
  gulp.watch('app/view/**/*', gulp.parallel('copyView'))
    .on('change', browser.reload); 
 
  gulp.watch('app/*', gulp.parallel('copyRoot'))
    .on('change', browser.reload);
})

gulp.task('serve', gulp.series('clean',
  gulp.parallel(    
    'copyRoot',
    'copyView',
    'copyController',
    'copyModel',
    'copyScripts',
    'copyStyles',
    'copyImages',
    'copyFont',
    'jshint',
    //'minifyStylesheets'
    ),
    'browser'));    

gulp.task('default', gulp.series('serve'));