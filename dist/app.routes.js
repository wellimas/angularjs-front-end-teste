app.config(function($routeProvider, $locationProvider){
    $routeProvider
    .when('/',{
        templateUrl : 'view/home.html',
        controller : 'AppController'
    })
    .when('/dados-plataformas',{
        templateUrl : 'view/dados-plataformas.html',
        controller : 'PlataformController'
    })
    .when('/dados-planos',{
        templateUrl : 'view/dados-planos.html',
        controller : 'PlanController'
    })          
    .when('/dados-cadastro',{
        templateUrl : 'view/dados-cadastro.html',
        controller : 'DadosCadastroController'
    })         
    .otherwise({ redirectTo: '/' });
});