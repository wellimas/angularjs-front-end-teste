app.controller('PlataformController', ['$scope', '$http', 'Plataforms', '$rootScope', function ($scope, $http, Plataforms, $rootScope) {
    $scope.store = this;
    $scope.store.selectedOption = {};
    $scope.store.plataformSelected = true;
    $scope.store.plataforms = {};
    $scope.store.selectedPlataform = {};

    $scope.changeSelectedOption = function () {
        $rootScope.plataformSelected = $scope.store.plataforms[$scope.store.selectedOption];
        $scope.store.plataformSelected = false;
    };

    Plataforms.listPlataforms($scope.store);
}]);

