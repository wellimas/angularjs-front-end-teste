app.controller('DadosCadastroController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.store = this;
    $scope.store.plataformSelected = $rootScope.plataformSelected;  
    $scope.store.planSelected = $rootScope.planSelected;
    $scope.store.tempNasc = {};
    $scope.store.person = {nome: '', email: '', cpf: '', nascimento: '', telefone: ''};    
    
    $scope.formAllGood = function () {
        return ($scope.store.person.nome && 
                $scope.store.person.email && 
                $scope.store.person.cpf && 
                $scope.store.person.nascimento && 
                $scope.store.person.telefone);
    }; 

    $scope.showData = function() {
        if(this.formAllGood())
        {
            var nascDate = $scope.store.person.nascimento.getDate();
            var nascMonth = $scope.store.person.nascimento.getMonth()+1;
            var nascFullYear = $scope.store.person.nascimento.getFullYear();
            $scope.store.tempNasc = nascDate+'/'+ nascMonth +'/'+nascFullYear;

            console.log('====== APRESENTAÇÃO DOS DADOS ======');
            console.log('====================================');
            console.log('NOME: ', $scope.store.person.nome);
            console.log('CPF: ', $scope.store.person.cpf);
            console.log('EMAIL: ', $scope.store.person.email);
            console.log('NASC: ', $scope.store.tempNasc);
            console.log('TEL: ', $scope.store.person.telefone);
            console.log('====================================');
            console.log('============ PLATAFORMA ============');
            console.log('====================================');          
            console.log('SKU: ', $scope.store.plataformSelected.sku);
            console.log('NOME: ', $scope.store.plataformSelected.nome);        
            console.log('DESC: ', $scope.store.plataformSelected.descricao);
            console.log('====================================');      
            console.log('============== PLANO ===============');
            console.log('====================================');         
            console.log('SKU: ', $scope.store.planSelected.sku);
            console.log('FRANQUIA: ', $scope.store.planSelected.franquia);        
            console.log('VALOR: R$', $scope.store.planSelected.valor);     
            console.log('====================================');    
        }           
    };    
}]);