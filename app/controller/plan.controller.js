app.controller('PlanController', ['$scope', '$http', 'Plans', 'Plataforms', '$rootScope', function ($scope, $http, Plans, Plataforms, $rootScope) {
    $scope.store = this;
    $scope.store.selectedOption = {};
    $scope.store.plataformSelected = $rootScope.plataformSelected;  

    $scope.changeSelectedRadio = function () {
        $rootScope.planSelected = $scope.store.plans[$scope.store.selectedOption];
        $scope.store.planSelected = true;
    };

    if ($rootScope.plataformSelected)
        Plans.listPlans($scope.store);
}]);

