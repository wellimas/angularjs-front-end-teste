app.factory('Plataforms', ['$http', function ($http) {

    var Plataforms = {};

    Plataforms.plataformSelected = {};


    Plataforms.listPlataforms = function (store) {
        $http.get('http://private-59658d-celulardireto2017.apiary-mock.com/plataformas')
            .then(
                function (response) {
                    Plataforms = response.data.plataformas;
                    store.plataforms = Plataforms;
                });
    };

    Plataforms.all = function () {
        $http.get('http://private-59658d-celulardireto2017.apiary-mock.com/plataformas')
            .then(
                function (response) {
                    Plataforms = response.data.plataformas;
                });
    };

    Plataforms.findBySku = function (sku) {
        return Plataforms.find(function (plataform) {
            return plataform.sku === sku;
        });
    };

    return Plataforms;
}]);

