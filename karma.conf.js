
module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      'node_modules/angular/angular.min.js',
      'node_modules/jasmine-core/lib/jasmine-core/jasmine.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'test/app.module.test.js',  
      'app/model/plataforms.model.js',      
      'app/model/plans.model.js',      
      'test/controller/plataform.spec.js',        
      'test/controller/plan.spec.js',      
    ],
    exclude: [],
    preprocessors: {},
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    concurrency: Infinity
  })
}
